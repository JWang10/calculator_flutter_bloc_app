import 'package:flutter/material.dart';

class CalculatorButton extends StatelessWidget {
  CalculatorButton({
    @required this.label,
    required this.onTap,
    required this.size,
    this.backgroundColor = Colors.white,
    this.labelColor = Colors.black,
  });
  final String? label;
  final VoidCallback onTap;
  final double size;
  final Color backgroundColor;
  final Color labelColor;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(color: Colors.grey, offset: Offset(1, 1), blurRadius: 2),
          ],
          borderRadius: BorderRadius.circular(size / 2),
          color: backgroundColor,
        ),

        // /// Alternative method
        // child: ElevatedButton(
        //   style: ElevatedButton.styleFrom(
        //     primary: backgroundColor,
        //     // onPrimary: Colors.grey,
        //     shape: CircleBorder(),
        //   ),
        //   onPressed: onTap,
        //   child: Text(
        //     label!,
        //     style: TextStyle(fontSize: 24, color: labelColor),
        //   ),
        // ),
        child: Material(
          borderRadius: BorderRadius.circular(size / 2),
          color: backgroundColor,
          // InkWell widget need to be used with Material widget together
          child: InkWell(
            // Only using itself is not work, no ripple effect
            customBorder: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(size / 2)),
            onTap: onTap,
            child: Center(
              child: Text(
                label!,
                style: TextStyle(fontSize: 24, color: labelColor),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
